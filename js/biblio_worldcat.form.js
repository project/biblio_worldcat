(function($){

  Drupal.behaviors.biblio_worldcat_form = {

    attach: function(context, settings) {
      var $search_container = $('#edit-biblio-worldcat-search-items');
      var $submit_button = $search_container.find('.form-submit');
      $search_container.find('.form-text').bind('keydown', function(ev){
        if (ev.which == 13) {
          ev.preventDefault();
          $submit_button.trigger('click');
        }
      });
    }

  }

})(jQuery);
