<?php
/**
 * Form definition for the initial WorldCat search fields. This form is
 * intended to be included as a step inside the Biblio node add form.
 */
function biblio_worldcat_search_fields_form($form, &$form_state) {
  // Ensure that an API key has been set.
  $api_key = variable_get('worldcat_api_key', '');
  if (empty($api_key)) {
    drupal_set_message(t('Please <a href="!url">set your WorldCat Open Search API key</a> before continuing with this form.', array('!url' => url('admin/config/services/worldcat'))), 'error');
  }
  // Add a textfield for keywords.
  $form['srw_kw'] = array(
    '#type' => 'textfield',
    '#title' => 'Keywords',
  );
  // Add a textfield for title.
  $form['srw_ti'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
  );
  // Add a textfield for Author.
  $form['srw_au'] = array(
    '#type' => 'textfield',
    '#title' => 'Author',
  );
  // Add a textfield for ISBN.
  $form['srw_bn'] = array(
    '#type' => 'textfield',
    '#title' => 'ISBN',
  );
  // Add a textfield for ISSN.
  $form['srw_in'] = array(
    '#type' => 'textfield',
    '#title' => 'ISSN',
  );
  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Search',
    '#submit' => array('biblio_worldcat_search_fields_form_submit'),
    '#limit_validation_errors' => array(),
  );

  return $form;
}

/**
 * "Search WorldCat" submit callback.
 *
 * Execute a WorldCat OpenSearch API request and store the results.
 */
function biblio_worldcat_search_fields_form_submit($form, &$form_state){
  $form_state['rebuild'] = TRUE;

  // Determine which page of results to retrieve.
  $form_state['results_per_page'] = 30;
  $form_state['results_page'] = (isset($form_state['results_page'])) ? $form_state['results_page'] : 0;
  $form_state['results_offset'] = ($form_state['results_per_page'] * $form_state['results_page']) + 1;

  // Retain the original search inputs so we can use them in pager requests.
  if (empty($form_state['search_inputs'])) {
    $form_state['search_inputs'] = $form_state['input'];
  }

  $criteria = array();
  $criteria['srw.kw'] = $form_state['search_inputs']['srw_kw'];
  $criteria['srw.ti'] = $form_state['search_inputs']['srw_ti'];
  $criteria['srw.au'] = $form_state['search_inputs']['srw_au'];
  $criteria['srw.bn'] = $form_state['search_inputs']['srw_bn'];
  $criteria['srw.in'] = $form_state['search_inputs']['srw_in'];

  $form_state['search_results'] = worldcat_search_api_execute_search($criteria, 'chicago', $form_state['results_per_page'], $form_state['results_offset']);
}

/**
 * Form definition to display a selectable table listing of search results.
 */
function biblio_worldcat_search_results_form($form, &$form_state) {
  if (empty($form_state['search_results'])) {
    drupal_set_message(t('No results found.'), 'warning');
    return FALSE;
  }

  // Go through results, populate the options array.
  $results = $form_state['search_results'];
  $options = array();
  foreach ($results->entry as $result) {
    $options[]['title'] = $result->content;
  }

  // Build the form array.
  $header = array(
    'title' => t('Showing results %start-%end', array('%start' => $form_state['results_offset'], '%end' => $form_state['results_offset'] + $form_state['results_per_page'] - 1)),
  );
  // Create selection table.
  $form['selection'] = array(
    '#header' => $header,
    '#type' =>  'tableselect',
    '#options' => $options,
    '#empty' => 'No results.',
    '#multiple' => FALSE,
  );
  // Add a submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Citation'),
    '#submit' => array('biblio_worldcat_search_results_form_submit'),
    '#limit_validation_errors' => array(),
  );
  // Add a qprevious button.
  if (!empty($form_state['results_page'])) {
    $form['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Previous results'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('biblio_worldcat_search_results_form_prev'),
    );
  }
  // Add a next button.
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next results'),
    '#executes_submit_callback' => TRUE,
    '#submit' => array('biblio_worldcat_search_results_form_next'),
  );

  return $form;
}

/**
 * Submit callback for the embedded search results form.
 *
 * Called after selecting an item from the search results.
 *
 * Wraps worldcat_search_api_search_results_form_submit().
 */
function biblio_worldcat_search_results_form_submit($form, &$form_state) {
  // Get the item selected.
  $citation_choice = $form_state['input']['selection'];

  // Find the actual record and get the OCLC id.
  $entry = $form_state['search_results']->entry[$citation_choice];
  $oclc = str_replace('http://worldcat.org/oclc/', '', $entry->id);

  // Set record to empty array
  $form_state['record'] = array();

  // Make request to retrieve the full record in Dublin Core format.
  $record = worldcat_search_api_get_record($oclc, 'dc');
  if ($record) {
    $form_state['record']['dc'] = $record;
  }

  // Make request to retrieve the full record in MARCXML format.
  $record = worldcat_search_api_get_record($oclc, 'marcxml');
  if ($record) {
    $form_state['record']['marc'] = $record;
  }

  $form_state['selection_biblio'] = worldcat_search_api_create($form_state['record']);

  // Insert the returned WorldCat values into the $form_state biblio fields.
  try {
    $form_state['input'] = array_merge($form_state['input'], $form_state['selection_biblio']);
    $form_state['biblio_type'] = $form_state['selection_biblio']['biblio_type'];
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    watchdog_exception('biblio_worldcat', $e->getMessage());
  }
  // Rebuild the form with the biblio values pre-filled.
  $form_state['rebuild'] = TRUE;
}

/**
 * 'Previous results' submit callback.
 *
 * Decrement the page and resubmit the search.
 */
function biblio_worldcat_search_results_form_prev($form, &$form_state) {
  $form_state['results_page']--;
  biblio_worldcat_search_fields_form_submit($form, $form_state);
}

/**
 * 'Next results' submit callback.
 *
 * Increment the page and resubmit the search.
 */
function biblio_worldcat_search_results_form_next($form, &$form_state) {
  $form_state['results_page']++;
  biblio_worldcat_search_fields_form_submit($form, $form_state);
}


/**
 * Prepare a node object corresponding to biblio node properties.
 *
 * @param array $record
 *   An array containing both DC and MARC versions of a record.
 *
 * @return array
 *   The selected citation, represented as an array of biblio fields.
 */
function worldcat_search_api_create($record = array()){
  $dc = (isset($record['dc']) ? $record['dc'] : FALSE);
  $marc = (isset($record['marc']) ? $record['marc'] : FALSE);
  $marc_fields = array();

  $leader = ($marc ? $marc->leader : '');

  $values = array();
  // Publication type & title
  $pubtype = $leader[6] . $leader[7];
  $values['biblio_type'] = _biblio_marc_type_map($pubtype);
  $values['biblio_lang'] = $dc->language;
  $values['title'] = (is_array($dc->title)) ? $dc->title[0] : $dc->title;
  $values['title'] = trim($values['title'], '/,.:; ');
  // Process each
  foreach ($marc->datafield as $delta => $field) {
    $tag = $field->{'@attributes'}->tag;
    switch ($tag) {
      case '020':
        $values['biblio_isbn'] = $field->subfield;
        break;
      case '022':
        $values['biblio_issn'] = $field->subfield;
        break;
      case '024':
        $values['biblio_other_number'] = $field->subfield;
        break;
      case '050': //LIBRARY OF CONGRESS CALL NUMBER
      case '055': //CLASSIFICATION NUMBERS ASSIGNED IN CANADA
      case '060': //NATIONAL LIBRARY OF MEDICINE CALL NUMBER
        $values['biblio_call_number'] = $field->subfield;
        break;
      case '130':
        $values['title'] = trim($field->subfield, '/,.:; ');
        break;
      case '210':
        $values['biblio_short_title'] = trim($field->subfield, '/,.:; ');
        break;
      case '245':
        // Let's use the DC record value for title instead of these two lines.
        // $value = (is_array($field->subfield)) ? $field->subfield[0] : $field->subfield;
        // $node->title = str_replace(' /', '', $value);
        break;
      case '246':
        $values['biblio_alternate_title'] = $field->subfield;
        break;
      case '250':
        $values['biblio_edition'] = $field->subfield;
        break;
      case '260':
        $values['biblio_place_published'] = trim($field->subfield[0], ',.;: ');
        $values['biblio_publisher'] = trim($field->subfield[1], ',.;: ');
        $values['biblio_date'] = trim($field->subfield[2], ',.;:© ');
        $values['biblio_year'] = $values['biblio_date'];
        break;
      case '300':
        $values['biblio_pages'] = trim($field->subfield[0], ',.;: ');
        break;
      case '490':
        $values['biblio_volume'] = $field->subfield;
        break;
      case ($tag >= 500 && $tag <= 599):
        $value = $field->subfield;
        if (!empty($value)) {
          $values['biblio_notes'] .= $value;
        }
        break;
      case '650':
      case '655':
        $value = (is_array($field->subfield)) ? $field->subfield[0] : $field->subfield;
        $value = trim($value, ',.;: ');
        $values['biblio_keywords'][$value] = $value . ',';
        break;
      case '100':
      case '700':
        $value = (is_array($field->subfield)) ? $field->subfield[0] : $field->subfield;
        $value = trim($value, ',.;: ');
        $values['biblio_contributors'][] = array(
          'name' => $value,
          'auth_category' => 1,
          'auth_type' => 1,
        );
        break;
    }
  }

  return $values;
}


